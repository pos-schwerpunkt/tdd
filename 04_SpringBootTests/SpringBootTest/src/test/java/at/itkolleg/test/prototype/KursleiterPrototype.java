package at.itkolleg.test.prototype;

import at.itkolleg.test.entities.Kurs;
import at.itkolleg.test.entities.Kursleiter;

public class KursleiterPrototype {

    public static Kursleiter sampleKursleiter() {
        Kursleiter kursleiter = new Kursleiter();
        Kurs kurs = new Kurs();
        kurs.setId(1L);
        kurs.setName("Test Driven Devolpment macht Spaß");
        kurs.setPlaetze(10);
        kurs.setBooked(true);
        kursleiter.setId(1L);
        kursleiter.setName("Max Mustermann");
        kursleiter.setKurse(kurs);
        return kursleiter;
    }

    public static Kursleiter sampleKursleiterNoKurs() {
        Kursleiter kursleiter = new Kursleiter();
        Kurs kurs = new Kurs();
        kurs.setId(1L);
        kurs.setName("Test Driven Devolpment macht Spaß");
        kurs.setPlaetze(10);
        kurs.setBooked(false);
        kursleiter.setId(1L);
        kursleiter.setName("Max Mustermann");
        kursleiter.setKurse(kurs);
        return kursleiter;
    }

//    public static Kursleiter sampleNewKl() {
//        Kursleiter kursleiter = new Kursleiter();
//        kursleiter.setId(1L);
//        kursleiter.setName("John Doe");
//        return kursleiter;
//    }


}

