package at.itkolleg.test.repositories;

import at.itkolleg.test.entities.Kursleiter;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import static at.itkolleg.test.prototype.KursleiterPrototype.sampleKursleiter;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
class KursleiterRepoTest {

    @Autowired
    private KursleiterRepo repo;


    @Test
    void getKursleiterById() {
        repo.save(sampleKursleiter());
        Kursleiter kl = repo.findById(sampleKursleiter().getId()).get();
        assertThat(kl).isNotNull();
        assertThat(kl.getName()).isEqualTo(sampleKursleiter().getName());
    }
}
