package at.itkolleg.test.services;

import at.itkolleg.test.entities.Kursleiter;
import at.itkolleg.test.exceptions.KursNotBookedException;
import at.itkolleg.test.exceptions.KursleiterNotFoundException;
import at.itkolleg.test.repositories.KursleiterRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static at.itkolleg.test.prototype.KursleiterPrototype.sampleKursleiter;
import static at.itkolleg.test.prototype.KursleiterPrototype.sampleKursleiterNoKurs;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class KursleiterServiceTest {

    private KursleiterRepo repo;
    private KursleiterService service;

    @BeforeEach
    void setUp() {
        repo = mock(KursleiterRepo.class);
        service = new KursleiterService(repo);
    }

    @Test
    void addNewKursleiter() throws KursNotBookedException {
        when(repo.save(any())).thenReturn(sampleKursleiter());
        Kursleiter newKl = service.addNewKursleiter(sampleKursleiter());
        assertThat(newKl).isNotNull();
        assertThat(newKl.getName()).isEqualTo(sampleKursleiter().getName());
    }

    @Test
    void saveKursleiterThrowsKursNotBooked() {
        assertThrows(KursNotBookedException.class,
                () -> service.addNewKursleiter(sampleKursleiterNoKurs()), "Der gewählte Kurs wurde noch nicht gebucht."
        );
    }

    @Test
    void getAllKursleiter() {
    }

    @Test
    void getKursleiterById() throws KursleiterNotFoundException {
        when(repo.findById(eq(1L))).thenReturn(java.util.Optional.of(sampleKursleiter()));
        Kursleiter k = service.getKursleiterById(1L);
        assertThat(k).isNotNull();
        assertThat(k.getName()).isEqualTo("Max Mustermann");
    }


    @Test
    void deleteKursleiter() {
    }
}
