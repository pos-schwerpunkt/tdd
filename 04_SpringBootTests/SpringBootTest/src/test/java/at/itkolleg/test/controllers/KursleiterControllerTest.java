package at.itkolleg.test.controllers;

import at.itkolleg.test.services.KursService;
import at.itkolleg.test.services.KursleiterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static at.itkolleg.test.prototype.KursleiterPrototype.sampleKursleiter;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class KursleiterControllerTest {

    MockMvc mockMvc;
    ObjectMapper mapper;
    KursleiterService service;
    KursService kursService;

    @BeforeEach
    void setUp() {
        service = mock(KursleiterService.class);
        kursService = mock(KursService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new KursleiterController(service, kursService)).build();
        mapper = new ObjectMapper();
    }

    @Test
    void getAllKursleiter() throws Exception {
        when(service.getAllKursleiter()).thenReturn(Collections.singletonList(sampleKursleiter()));
        mockMvc.perform(get("/kursleiter")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapper.writeValueAsString(Collections.singletonList(sampleKursleiter()))))
                .andExpect(status().isOk());
    }

    @Test
    void getLeiterById() {
    }

    @Test
    void addNewKursleiter() throws Exception {
        when(service.addNewKursleiter(any())).thenReturn(sampleKursleiter());
        mockMvc.perform(post("/kursleiter")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(sampleKursleiter())))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(mapper.writeValueAsString(sampleKursleiter())));
    }

    @Test
    void deleteKursleiter() {
    }
}
