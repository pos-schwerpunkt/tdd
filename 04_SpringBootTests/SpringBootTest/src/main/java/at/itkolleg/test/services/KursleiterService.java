package at.itkolleg.test.services;

import at.itkolleg.test.entities.Kurs;
import at.itkolleg.test.entities.Kursleiter;
import at.itkolleg.test.exceptions.KursNotBookedException;
import at.itkolleg.test.exceptions.KursleiterNotFoundException;
import at.itkolleg.test.repositories.KursleiterRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class KursleiterService {

    private final KursleiterRepo repo;

    public KursleiterService(KursleiterRepo repo) {
        this.repo = repo;
    }

    public List<Kursleiter> getAllKursleiter() {
        return repo.findAll();
    }

    public Kursleiter getKursleiterById(Long id) throws KursleiterNotFoundException {
        Optional<Kursleiter> kursleiterOptional = repo.findById(id);
        if(kursleiterOptional.isPresent()) {
            return kursleiterOptional.get();
        } else {
            throw new KursleiterNotFoundException();
        }
    }

    public List<Kursleiter> getAllKursleiterByKurs(Kurs kurs) {
        return repo.getKursleiterByKurse(kurs);
    }

    public Kursleiter addNewKursleiter(Kursleiter kursleiter) throws KursNotBookedException {
        if(kursleiter.getKurse().getBooked()) {
            return repo.save(kursleiter);
        } else {
            throw new KursNotBookedException();
        }
    }

    public void deleteKursleiter(Long id) {
        repo.deleteById(id);
    }

    public void updateKursleiter(Kursleiter kursleiter) throws KursleiterNotFoundException {
        Optional<Kursleiter> kursleiterOptional = repo.findById(kursleiter.getId());
        if(kursleiterOptional.isPresent()) {
            Kursleiter newKursleiter = kursleiterOptional.get();
            newKursleiter.setId(kursleiter.getId());
            newKursleiter.setName(kursleiter.getName());
            repo.save(newKursleiter);
        } else {
            throw new KursleiterNotFoundException();
        }
    }
}
