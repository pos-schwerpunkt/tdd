package at.itkolleg.test.repositories;

import at.itkolleg.test.entities.Kurs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KursRepo extends JpaRepository<Kurs, Long> {
}
