package at.itkolleg.test.controllers;

import at.itkolleg.test.entities.Kurs;
import at.itkolleg.test.entities.Kursleiter;
import at.itkolleg.test.exceptions.KursNotBookedException;
import at.itkolleg.test.exceptions.KursNotFoundException;
import at.itkolleg.test.exceptions.KursleiterNotFoundException;
import at.itkolleg.test.services.KursService;
import at.itkolleg.test.services.KursleiterService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class KursleiterController {

    private final KursleiterService service;
    private final KursService kursService;
    private final String LEITER = "/kursleiter";
    private final String LEITERID = "/kursleiter/{id}";

    public KursleiterController(KursleiterService service, KursService kursService) {
        this.service = service;
        this.kursService = kursService;
    }

    @GetMapping(LEITER)
    public ResponseEntity<List<Kursleiter>> getAllKursleiter() {
        return ResponseEntity.ok(service.getAllKursleiter());
    }

    @GetMapping(LEITERID)
    public ResponseEntity<Kursleiter> getLeiterById(@PathVariable Long id) throws KursleiterNotFoundException {
        return new ResponseEntity<>(service.getKursleiterById(id), HttpStatus.OK);
    }

    @GetMapping("/kursleiter/kurs/{id}")
    public ResponseEntity<List<Kursleiter>> getKursleiterByKursId(@PathVariable Long id) throws KursNotFoundException {
        Kurs kurs = kursService.getKursById(id);
        List<Kursleiter> KursleiterList = service.getAllKursleiterByKurs(kurs);
        return new ResponseEntity<>(KursleiterList, HttpStatus.OK);
    }

    @PostMapping(LEITER)
    public ResponseEntity<Kursleiter> addNewKursleiter(@Valid @RequestBody Kursleiter k) throws URISyntaxException, KursNotBookedException {
        Kursleiter newKursleiter = service.addNewKursleiter(k);
        return ResponseEntity.created(new URI(LEITER + newKursleiter.getId())).body(k);
    }

    @DeleteMapping(LEITERID)
    public ResponseEntity<Kursleiter> deleteKursleiter(@PathVariable Long id) {
        service.deleteKursleiter(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(LEITER)
    public ResponseEntity<Kursleiter> updateKursleiter(@Valid @RequestBody Kursleiter k) throws KursleiterNotFoundException {
        service.updateKursleiter(k);
        return ResponseEntity.ok().build();
    }


}
