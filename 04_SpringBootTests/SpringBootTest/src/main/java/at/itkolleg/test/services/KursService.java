package at.itkolleg.test.services;

import at.itkolleg.test.entities.Kurs;
import at.itkolleg.test.exceptions.KursNotFoundException;
import at.itkolleg.test.repositories.KursRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class KursService {

    private final KursRepo repo;

    public KursService(KursRepo repo) {
        this.repo = repo;
    }

    public List<Kurs> getAllCourses() {
        return repo.findAll();
    }

    public Kurs getKursById(Long id) throws KursNotFoundException {
        Optional<Kurs> kursOptional = repo.findById(id);
        if(kursOptional.isPresent()) {
            return kursOptional.get();
        } else {
            throw new KursNotFoundException();
        }
    }

    public Kurs addNewKurs(Kurs kurs) {
        return repo.save(kurs);
    }

    public void deleteKurs(Long id) {
        repo.deleteById(id);
    }

    public void updateKurs(Kurs kurs) throws KursNotFoundException {
        Optional<Kurs> kursOptional = repo.findById(kurs.getId());
        if(kursOptional.isPresent()) {
            Kurs newKurs = kursOptional.get();
            newKurs.setId(kurs.getId());
            newKurs.setName(kurs.getName());
            newKurs.setPlaetze(kurs.getPlaetze());
            newKurs.setBooked(kurs.getBooked());
            repo.save(newKurs);
        } else {
            throw new KursNotFoundException();
        }
    }
}
