package at.itkolleg.test.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Kurs {

    public Kurs() {
    }

    @Id
    @GeneratedValue
    @Column(name = "kurs_id")
    private Long id;
    private String name;
    private Integer plaetze;
    private Boolean booked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPlaetze() {
        return plaetze;
    }

    public void setPlaetze(Integer plaetze) {
        this.plaetze = plaetze;
    }

    public Boolean getBooked() {
        return booked;
    }

    public void setBooked(Boolean booked) {
        this.booked = booked;
    }
}
