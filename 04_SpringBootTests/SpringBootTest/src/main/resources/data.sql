INSERT INTO KURS VALUES ( 1, true, 'Webdesign', 10);
INSERT INTO KURS VALUES ( 2, false, 'PHP-Programmierung', 5 );
INSERT INTO KURS VALUES ( 3, true, 'Backend-Entwicklung', 5 );

INSERT INTO KURSLEITER VALUES ( 1, 'John Doe', 3 );
INSERT INTO KURSLEITER VALUES ( 2, 'Max Mustermann', 1 );
INSERT INTO KURSLEITER VALUES ( 3, 'Blake Wyatt', 2 );