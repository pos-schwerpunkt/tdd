package at.itkolleg;

import at.itkolleg.mockito.Student;
import at.itkolleg.mockito.StudentService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class StudentTest {

    StudentService service = Mockito.mock(StudentService.class);

    Student student = new Student(service);
    @Test
    void testAverage() {
        Mockito.when(service.getTotalMarks()).thenReturn(500);
        Mockito.when(service.getTotalStudents()).thenReturn(10);
        Assertions.assertEquals(50, student.getAverageMarks());
    }
}
