package at.itkolleg;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TestVorstellung {
    private KinoSaal ks1, ks2, ks3;
    private Vorstellung v1, v2, v3, v4;

    @BeforeEach
    void setup() {
        Map<Character, Integer> seats = new HashMap<>();
        seats.put('A', 20);
        seats.put('B', 30);
        seats.put('C', 40);
        seats.put('D', 50);

        ks1 = new KinoSaal("Metropol", seats);
        ks2 = new KinoSaal("Cineplex", seats);
        ks3 = new KinoSaal("Cineplex", seats);
        v1 = new Vorstellung(ks1, Zeitfenster.ABEND, LocalDate.now(), "Agent Ranjid", 23);
        v2 = new Vorstellung(ks2, Zeitfenster.ABEND, LocalDate.now(), "Zurück in die Zukunft", 14);
        v3 = new Vorstellung(ks2, Zeitfenster.ABEND, LocalDate.now(), "Letzter Countdown", 31);
        v4 = new Vorstellung(ks3, Zeitfenster.NACHMITTAG, LocalDate.now(), "Zurück in die Zukunft", 17);
    }

    @Test
    void gleicherSaal() {
        assertNotEquals(v1, v2);
        assertNotEquals(v2, v4);
        assertNotEquals(v1, v4);
        assertEquals(v2, v3);
    }

    @Test
    void kaufeTicketZuWenigGeld() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            v1.kaufeTicket('A', 2, 0);
        });

        String expectedMessage = "Nicht ausreichend";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void kaufeTicketGenugGeld() {
        assertNotNull(v1.kaufeTicket('A', 1, 50));
    }

    @Test
    void kaufeTicketPlatzDoesntExist() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            v1.kaufeTicket('Z', 999, 50);
        });

        String expectedMessage = "existiert nicht";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage), "Der gewuenschte Platz existiert nicht!");
    }

    @Test
    void kaufeTicketPlatzExistiert() {
        assertNotNull(v1.kaufeTicket('A', 1, 50));
    }

    @Test
    void kaufeTicketPlatzBelegt() {
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            v1.kaufeTicket('A', 1, 50);
            v1.kaufeTicket('A', 1, 50);
        });

        String expectedMessage = "bereits belegt";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void kaufeTicketPlatzNichtBelegt() {
        assertNotNull(v1.kaufeTicket('A', 1, 50));
    }

}
