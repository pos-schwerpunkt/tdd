package at.itkolleg;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestKinoverwaltung {
    private KinoSaal ks1, ks2, ks3;
    private Vorstellung v1, v2, v3, v4;
    private KinoVerwaltung verwaltung = new KinoVerwaltung();


    @BeforeEach
    void setup() {
        Map<Character, Integer> seats = new HashMap<>();
        seats.put('A', 20);
        seats.put('B', 30);
        seats.put('C', 40);
        seats.put('D', 50);

        ks1 = new KinoSaal("Metropol", seats);
        ks2 = new KinoSaal("Cineplex", seats);
        ks3 = new KinoSaal("Cineplex", seats);
        v1 = new Vorstellung(ks1, Zeitfenster.ABEND, LocalDate.now(), "Agent Ranjid", 23);
        v2 = new Vorstellung(ks2, Zeitfenster.ABEND, LocalDate.now(), "Zurück in die Zukunft", 14);
        v3 = new Vorstellung(ks2, Zeitfenster.ABEND, LocalDate.now(), "Letzter Countdown", 31);
        v4 = new Vorstellung(ks3, Zeitfenster.NACHMITTAG, LocalDate.now(), "Zurück in die Zukunft", 17);
        verwaltung.einplanenVorstellung(v1);
    }

    @Test
    void vorstellungEinplanen() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            verwaltung.einplanenVorstellung(v1);
        });

        String expectedMessage = "bereits eingeplant";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void kaufeTicket() {
        verwaltung.kaufeTicket(v1, 'A', 1, 50);
    }



}
