package at.itkolleg;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

public class AdvancedTests {

    KinoVerwaltung verwaltung = new KinoVerwaltung();
    private KinoSaal ks;
    private Vorstellung v1, v2;

    @BeforeEach
    public void setup() {
        Map<Character, Integer> seats = new HashMap<>();
        seats.put('A', 10);
        seats.put('B', 10);
        seats.put('C', 15);
        ks = new KinoSaal("Grand Wall", seats);
        v1 = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.now(), "Süperagent Ranjid", 15);
        v2 = new Vorstellung(ks, Zeitfenster.NACHMITTAG, LocalDate.now(), "Zurück in die Zukunft", 30);
    }


    //Teil 1
    @Test
    void eineVorstellungEinplanen() {
        verwaltung.einplanenVorstellung(v1);
        assertEquals(v1, verwaltung.getVorstellungen().get(0),
                "Vorstellung wurde nicht richtig erfasst.");
        assertEquals(1, verwaltung.getVorstellungen().size(),
                "Vorstellung konnte nicht gespeichert werden.");

    }

    // Teil 2
    @Test
    void zweiVorstellungenEinplanen() {
        verwaltung.einplanenVorstellung(v1);
        verwaltung.einplanenVorstellung(v2);
        assertEquals(2, verwaltung.getVorstellungen().size(),
                "Anzahl der Vorstellungen wurde nicht richtig erfasst.");
    }

    // Teil 3
    @Test
    void zweiGleicheVorstellungen() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            verwaltung.einplanenVorstellung(v1);
            verwaltung.einplanenVorstellung(v1);
        });
        String expectedMessage = "bereits eingeplant";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    // Teil 4
    @ParameterizedTest
    @CsvSource({"5", "2", "7"})
    void MyFirstParameterizedTest(int test) {
        assertTrue(test > 4, "Einer oder mehrere Tests sind fehlgeschlagen.");
    }

    @ParameterizedTest(name = "Reihe: {0}, Platz: {1}, Geld: {2}")
    @CsvSource({"C, 5, 15", "A, 3, 20", "B, 8, 0", "D, 1, 15"}) // richtiges Ticket, zu viel Geld, kein Geld, ungültiger Sitzplatz
    void ticketKauf(char reihe, int platz, int geld) {
        verwaltung.einplanenVorstellung(v1);

        Ticket ticket = verwaltung.kaufeTicket(v1, reihe, platz, geld);

        assertEquals("Grand Wall", ticket.getSaal(), "Falscher Saal ausgewählt.");
        assertEquals(Zeitfenster.ABEND, ticket.getZeitfenster(), "Ungültiges Zeitfenster.");
        assertEquals(LocalDate.now(), ticket.getDatum(), "Ungültiges Datum.");
        assertEquals(reihe, ticket.getReihe(), "Ungültige Reihe");
        assertEquals(platz, ticket.getPlatz(), "Ungültiger Platz");
    }

    // Teil 5
    @TestFactory
    Collection<DynamicTest> dynamicTestsFromCollection() {
        return Collections.singletonList(
                dynamicTest("1st dynamic test", () -> assertEquals(25, Math.pow(5,2)))
        );
    }
    // Quelle: https://training.cherriz.de/cherriz-training/1.0.0/testen/junit5.html
    @TestFactory
    public Stream<DynamicTest> DynkaufeTicket() {
        verwaltung.einplanenVorstellung(v1);
        verwaltung.einplanenVorstellung(v2);

        return new Random(999)
                .ints(0, 1000)
                .limit(100)
                .mapToObj(i -> {
                    Vorstellung vorstellung = verwaltung.getVorstellungen().get(i % 2);
                    char reihe = (char) ((i % 10) + 65);
                    int platz = i % 15;
                    int geld = i % 50;

                    return DynamicTest.dynamicTest(vorstellung.getFilm() + ", " + reihe + platz + ", " + geld + "€",
                            () -> assertDoesNotThrow(() -> {
                                try {
                                    verwaltung.kaufeTicket(v1,
                                            reihe,
                                            platz,
                                            geld);
                                } catch (IllegalArgumentException e) {
                                    boolean errGeld = "Nicht ausreichend Geld.".equals(e.getMessage());
                                    boolean errPlatz = e.getMessage()
                                            .contains("existiert nicht");
                                    assertTrue(errGeld || errPlatz);
                                } catch (IllegalStateException e) {
                                    assertTrue(e.getMessage()
                                            .contains("ist bereits belegt."));
                                }
                            }));
                });
    }


}
