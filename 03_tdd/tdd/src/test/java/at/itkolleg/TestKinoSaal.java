package at.itkolleg;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TestKinoSaal {

    private KinoSaal ks1, ks2, ks3;

    @BeforeEach
    void setUp() {
        Map<Character, Integer> seats = new HashMap<>();
        seats.put('A', 20);
        seats.put('B', 30);
        seats.put('C', 40);
        seats.put('D', 50);

        ks1 = new KinoSaal("Metropol", seats);
        ks2 = new KinoSaal("Cineplex", seats);
        ks3 = new KinoSaal("Cineplex", seats);
    }

    @Test
    void compareKinosaal() {
        assertNotEquals(ks1, ks2);
        assertEquals(ks3, ks2);
//        assertFalse(ks.equals(ks2));
//        assertTrue(ks2.equals(ks3));

    }

    @Test
    void checkPlaetze() {
        assertTrue(ks1.pruefePlatz('A', 10));
    }

    @Test
    void checkName() {
        assertEquals("Metropol", ks1.getName());
        assertEquals("Cineplex", ks2.getName());
        assertEquals("Cineplex", ks3.getName());
    }
}
