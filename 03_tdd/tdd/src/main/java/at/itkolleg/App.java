package at.itkolleg;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Dieses Beispiel stammt aus http://www.cherriz.de/training/index.php?chapter=5.4
 */
public class App {
    private static final KinoVerwaltung verwaltung = new KinoVerwaltung();

    public static void main( String[] args ) {
        //Saal anlegen
        Map<Character,Integer> map = new HashMap<>();
        map.put('A',10);
        map.put('B',10);
        map.put('C',15);
        KinoSaal ks = new KinoSaal("LadyX",map);

        Map<Character, Integer> seats = new HashMap<>();
        seats.put('A', 20);
        seats.put('B', 30);
        seats.put('C', 40);
        seats.put('D', 50);
        KinoSaal ks2 = new KinoSaal("Cineplex", seats);

        //Platz prüfen
        System.out.println(ks.pruefePlatz('A',11));
        System.out.println(ks.pruefePlatz('A',10));
        System.out.println(ks.pruefePlatz('B',10));
        System.out.println(ks.pruefePlatz('C',14));

        System.out.println("Saal 2: ");
        System.out.println(ks2.pruefePlatz('A',0));
        System.out.println(ks2.pruefePlatz('A',1));
        System.out.println(ks2.pruefePlatz('A',-1));
        System.out.println(ks2.pruefePlatz('B',30));
        System.out.println(ks2.pruefePlatz('C',40));

        if(ks.equals(ks2)) {
            System.out.println("Kinosaal 1 == Kinosaal 2");
        } else {
            System.out.println("Kinosaal != Kinosaal 2");
        }

        //Vorstellung anlegen
        System.out.println("Neue Vorstellungen anlegen:");
        Vorstellung v1 = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.now(), "Agent Ranjid", 23);
        //Vorstellung v2 = new Vorstellung(ks, Zeitfenster.ABEND, LocalDate.now(), "Zurück in die Zukunft", 14); // gleicher Saal zur gleichen Zeit --> Fehler!
        Vorstellung v2 = new Vorstellung(ks2, Zeitfenster.NACHMITTAG, LocalDate.now(), "Zurück in die Zukunft", 14);

        //Ticket erstellen
        System.out.println("Neues Ticket erstellen");
        Ticket t = new Ticket("Saal 1", Zeitfenster.NACHMITTAG, LocalDate.now(), 'A', 4);

        System.out.println("Ticket kaufen:");
        //System.out.println(v1.kaufeTicket('A', 1, 50));
        v1.kaufeTicket('A', 2, 50);
        v2.kaufeTicket('A', 3, 14);

        System.out.println("Vorstellungen einplanen:");
        verwaltung.einplanenVorstellung(v1);
        verwaltung.einplanenVorstellung(v2);

        System.out.println(verwaltung.getVorstellungen());  // returns object-reference!
//        System.out.println(verwaltung.kaufeTicket(v1,'A', 4, 23));


    }
}
