package at.itkolleg.mockito;

public class Student {
    StudentService service;

    public Student(StudentService service) {
        this.service = service;
    }

    public int getAverageMarks() {
        return service.getTotalMarks() / service.getTotalStudents();
    }
}
