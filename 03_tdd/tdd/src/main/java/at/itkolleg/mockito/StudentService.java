package at.itkolleg.mockito;

public interface StudentService {
    int getTotalMarks();
    int getTotalStudents();
}
